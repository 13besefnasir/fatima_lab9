OBJS = polymorphism.o 

prog1 : $(OBJS)
	gcc -o prog1 $(OBJS) 

polymorphism.o : polymorphism.c 
	gcc -c polymorphism.c

clean:
	rm $(OBJS)
